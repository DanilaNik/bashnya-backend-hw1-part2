package parser

import (
	"strconv"
	"strings"
)

func ParseExpression(expression string) (expressionElements []string) {
	expression = strings.Replace(expression, " ", "", -1)
	var number string = ""
	for _, v := range expression {
		_, err := strconv.Atoi(string(v))
		if err != nil {
			if number != "" {
				expressionElements = append(expressionElements, number)
				number = ""
			}
			expressionElements = append(expressionElements, string(v))
		} else {
			number += string(v)
		}
	}
	if number != "" {
		expressionElements = append(expressionElements, number)
	}
	return
}
