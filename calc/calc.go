package main

import (
	"calc/calculator"
	"calc/parser"
	"fmt"
	"log"
	"os"
)

func main() {
	str := os.Args[1]
	expression := parser.ParseExpression(str)
	answer, err := calculator.Calculate(expression)
	if err != nil {
		log.Fatalf("%v", err)
	}
	fmt.Println(answer)
}
