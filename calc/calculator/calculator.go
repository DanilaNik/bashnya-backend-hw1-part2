package calculator

import (
	"errors"
	"strconv"
)

func transform(expressionElements []string) []string {
	for i := 1; i < len(expressionElements)-1; i++ {
		_, err := strconv.Atoi(expressionElements[i])
		if err == nil && expressionElements[i-1] == "(" && expressionElements[i+1] == ")" {
			expressionElements = removeElement(expressionElements, i+1)
			expressionElements = removeElement(expressionElements, i-1)
		}
	}
	for i := 1; i < len(expressionElements); i++ {
		_, err := strconv.Atoi(expressionElements[i])
		if err != nil {
			continue
		}
		if expressionElements[i-1] == "-" {
			n, _ := strconv.Atoi(expressionElements[i])
			expressionElements[i] = strconv.Itoa(-1 * n)
			_, err := strconv.Atoi(expressionElements[i-2])
			if err == nil || expressionElements[i-2] == ")" {
				expressionElements[i-1] = "+"
			} else {
				expressionElements = removeElement(expressionElements, i-1)
			}
		}
	}
	return expressionElements
}

func multiplicationDivision(expressionElements []string) []string {
	for i := 1; i < len(expressionElements)-1; i++ {
		element1, err := strconv.Atoi(expressionElements[i-1])
		element2, err1 := strconv.Atoi(expressionElements[i+1])
		if (expressionElements[i] == "*" || expressionElements[i] == "/") && err == nil && err1 == nil {
			if expressionElements[i] == "*" {
				expressionElements[i] = strconv.Itoa(element1 * element2)
			} else {
				expressionElements[i] = strconv.Itoa(element1 / element2)
			}
			expressionElements = removeElement(expressionElements, i+1)
			expressionElements = removeElement(expressionElements, i-1)
			break
		}
	}
	return expressionElements
}

func sumDifference(expressionElements []string) []string {
	for i := 1; i < len(expressionElements)-1; i++ {
		element1, err := strconv.Atoi(expressionElements[i-1])
		element2, err1 := strconv.Atoi(expressionElements[i+1])
		if (expressionElements[i] == "+" || expressionElements[i] == "-") && err == nil && err1 == nil {
			if expressionElements[i] == "+" {
				expressionElements[i] = strconv.Itoa(element1 + element2)
			} else {
				expressionElements[i] = strconv.Itoa(element1 - element2)
			}
			expressionElements = removeElement(expressionElements, i+1)
			expressionElements = removeElement(expressionElements, i-1)
			break
		}
	}
	return expressionElements
}

func removeElement(slice []string, s int) []string {
	return append(slice[:s], slice[s+1:]...)
}

func Calculate(expressionElements []string) (result int, err error) {
	var operationNumber int
	for _, v := range expressionElements {
		_, elementsErr := strconv.Atoi(v)
		if elementsErr != nil && v != "(" && v != ")" {
			operationNumber++
		}
	}
	for i := 0; i < operationNumber; i++ {
		expressionElements = multiplicationDivision(expressionElements)
		expressionElements = transform(expressionElements)
		expressionElements = sumDifference(expressionElements)
		expressionElements = transform(expressionElements)
		if len(expressionElements) == 1 {
			result, _ = strconv.Atoi(expressionElements[0])
			return result, nil
		}
	}
	return 0, errors.New("Incorrect expression!")
}
