package calculator

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCalculate(t *testing.T) {
	testTable := []struct {
		expression []string
		expected   int
	}{
		{
			expression: []string{"1", "+", "3"},
			expected:   4,
		},
		{
			expression: []string{"4", "-", "1"},
			expected:   3,
		},
		{
			expression: []string{"7", "*", "3"},
			expected:   21,
		},
		{
			expression: []string{"49", "/", "7"},
			expected:   7,
		},
		{
			expression: []string{"(", "2", "-", "4", ")", "*", "(", "3", "*", "2", ")"},
			expected:   -12,
		},
		{
			expression: []string{"(", "1", "-", "4", ")", "*", "5"},
			expected:   -15,
		},
		{
			expression: []string{"(", "1", "+", "4", ")", "*", "5"},
			expected:   25,
		},
		{
			expression: []string{"(", "(", "1", "*", "4", ")", "+", "10", ")", "*", "5"},
			expected:   70,
		},
		{
			expression: []string{"(", "-", "(", "1", "*", "4", ")", "+", "10", ")", "*", "5"},
			expected:   30,
		},
	}

	for _, testCase := range testTable {
		result, _ := Calculate(testCase.expression)
		assert.Equal(t, testCase.expected, result, fmt.Sprintf("Incorrect result!"))
	}
}
